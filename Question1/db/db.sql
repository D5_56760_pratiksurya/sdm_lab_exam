create table Movie(movie_id INTEGER Primary key Auto_increment,
    movie_title varchar(30),
    movie_release_date DATE,
    movie_time varchar(30),
    director_name varchar(30)) ;

insert into Movie values(0,"Spiderman","2020-02-10","11.20","John Doe");
insert into Movie values(0,"Inception","2010-05-10","12.20","Leonardo");
insert into Movie values(0,"tamasha","2020-02-10","11.20","Ranbir");
insert into Movie values(0,"Rockstar","2020-02-10","11.20","Arijit");