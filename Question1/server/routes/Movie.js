const express = require("express")
const db = require("../db")
const utils = require("../utils")


const router = express.Router()


router.get("/getByName", (request, response) => {
    const { title } = request.body;

    const sql = `select * from Movie where movie_title='${title}'`;
    db.execute(sql, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})


router.post("/add", (request, response) => {
    const { title, releaseDate, time, directorName } = request.body;

    const sql = `insert into Movie values(0,'${title}','${releaseDate}','${time}','${directorName}')`;
    db.execute(sql, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})

router.put("/update/:id", (request, response) => {
    const { releaseDate, time } = request.body;
    const { id } = request.params

    const sql = `update Movie set movie_release_date='${releaseDate}',movie_time='${time}' where movie_id=${id}`;
    db.execute(sql, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})

router.delete("/delete/:id", (request, response) => {
    const { id } = request.params
    const sql = `delete from Movie where movie_id=${id}`;
    db.execute(sql, (error, result) => {
        response.send(utils.createResult(error, result))
    })

})



module.exports = router 