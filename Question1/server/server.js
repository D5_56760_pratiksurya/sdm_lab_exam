const express=require("express")
const movieRouter=require("./routes/Movie")
const cors=require("cors")
const app=express()

app.use(cors("*"))
app.use(express.json())
app.use("/movie",movieRouter)



app.listen(4000,'0.0.0.0',()=>{
    console.log("server started on port 4000");
})