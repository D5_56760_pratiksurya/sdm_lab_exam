const mysql = require("mysql2")

const pool = mysql.createPool({
    host: "sdmdb",
    user: "root",
    password: "root",
    database: "sdm_lab",
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
})
module.exports = pool